#/bin/bash
echo "###############################";
echo "start update of symfony project";
echo "###############################";
git pull origin master --strategy-option theirs
composer --version
yarn --version

yarn run encore production
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
php bin/console cache:clear -eprod
echo "###########";
echo "update done";
echo "###########";