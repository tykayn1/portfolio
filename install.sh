#!/bin/bash

echo "################################";
echo "start INSTALL of symfony project";
echo "################################";
echo "checking requirements";

sudo apt install acl -y
echo "fix file permissions";
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo chmod 777 -R public/build/
sudo npm i -g yarn


composer --version
yarn --version
acl --varsion

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php
php -r "unlink('composer-setup.php');"


echo "installing symfony project for ubuntu environnement";
composer install && yarn install;
yarn run encore dev

echo "update database";
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force

echo "################################";
echo "end INSTALL of symfony project";
echo "################################";